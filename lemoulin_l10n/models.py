from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.utils.translation import get_language, ugettext_lazy as _


class MultilingualQuerySet(models.QuerySet):
	def for_language(self, language_code):
		return self.filter(language_code=language_code)

	def for_current_language(self):
		language_code = get_language()[:2]

		return self.for_language(language_code=language_code)


class MultilingualModelManager(models.Manager):
	def get_queryset(self):
		return MultilingualQuerySet(self.model, using=self._db)


class MultilingualModel(models.Model):
	"""
	Adds a language_code and a method to objects to retrieve content for specific language
	"""

	language_code = models.CharField(verbose_name=_("Language"), max_length=10, choices=settings.LANGUAGES, default=settings.LANGUAGE_CODE, )

	objects = MultilingualQuerySet.as_manager()

	class Meta:
		abstract = True


class ModelWithTranslations(models.Model):
	"""
	Adds methods to lookup translated fields
	"""

	def lookup_translation(self, attr, current_language=True, language_code=None, get_default=False, default_language=settings.LANGUAGE_CODE):
		"""
		Get a translated attribute by language in external table.

		If specific language isn't found, returns empty string
		"""
		if current_language:
			language_code = get_language()[:2]

		try:
			return getattr(self.translations.for_language(language_code=language_code).get(), attr)
		except:
			if get_default:
				return self.lookup_translation(attr=attr, language_code=default_language, get_default=False)
			else:
				return ""

	def lookup_simple_translation(self, attr, current_language=True, language_code=None, get_default=False, default_language=settings.LANGUAGE_CODE):
		"""
		Gets attribute from current table in the form of {attribut}_{language_code}
		"""
		if current_language:
			language_code = get_language()[:2]

		try:
			return eval("self.{attr}_{language_code}".format(attr=attr, language_code=language_code))
		except:
			if get_default:
				try:
					return eval("self.{attr}_{language_code}".format(attr=attr, language_code=default_language, get_default=False))
				except:
					try:
						return eval("self.{attr}".format(attr=attr))
					except:
						return ""
			else:
				return ""

	class Meta:
		abstract = True
