from django import template
from django.core.urlresolvers import resolve, reverse
from django.utils.translation import activate, get_language

register = template.Library()

@register.filter()
def chlocale(path, lang_code):
	"""
	Returns a path in specified language

	ex:
	<a href="{{ request.path|chlocale:lang_code }}">{{ lang_code }}</a>
	"""
	# Get current language
	cur_lang_code = get_language()

	# Resolve path
	resolved_view = resolve(path)

	# Activate specified language
	activate(lang_code)

	# Resolve URL in specified language
	url = reverse(viewname=resolved_view.view_name, args=resolved_view.args, kwargs=resolved_view.kwargs)

	# Reactivate current language
	activate(cur_lang_code)

	return url
