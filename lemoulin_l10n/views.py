from __future__ import unicode_literals

class LanguageMixin(object):
	"""
	Returns content for current language
	"""
	def get_queryset(self):
		return super(LanguageMixin, self).get_queryset().for_current_language()
