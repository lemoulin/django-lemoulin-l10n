==============
Le Moulin l10n
==============

*******
CHANGES
*******

.. contents:: Versions

v0.1
====
Initial release

v0.3
====
New Packaging

v0.31
====
Fixed reverse url issues
