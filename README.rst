===============
Le Moulin l10n
===============

A collection of localization utilities for Django.

.. contents::

Getting started
===============

Requirements
------------

Le Moulin Utils requires:

- Python 3.4
- Django 1.7 or greater

Installation
------------

You can get Le Moulin Utils by using pip::

    $ pip install -e git+https://bitbucket.org/lemoulin/django-lemoulin-l10n.git#egg=lemoulin_l10n

To enable `lemoulin_l10n` in your project you need to add it to `INSTALLED_APPS` in your projects
`settings.py` file::

    INSTALLED_APPS = (
        ...
        'lemoulin_l10n',
        ...
    )

Usage
=====

