from setuptools import setup, find_packages

version = __import__('lemoulin_utils').__version__

packages = find_packages()

setup(
    name='django-lemoulin-l10n',
    packages=packages,
    version=version,
    description='A collection of localization utilities for Django.',
    author='Yanik Proulx',
    author_email='yanikproulx@lemoulin.co',
    url='https://bitbucket.org/lemoulin/django-lemoulin-l10n',
    download_url='https://bitbucket.org/lemoulin/django-lemoulin-l10n',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
    scripts=[],
    license='LICENSE.txt',
    long_description=open('README.rst').read(),
    install_requires=[
        "Django >= 1.7",
    ],
    include_package_data = True,
)
